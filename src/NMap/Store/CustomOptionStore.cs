using System.Collections.Generic;
using NMap.Exceptions;

namespace NMap.Store
{
	public class CustomOptionStore
	{
		private int _keyIndex = 1;
		private readonly IDictionary<string, int> _content = new Dictionary<string, int>(); 

		public int Get(string key)
		{
			int result;
			if(_content.TryGetValue(key, out result)) return result;
			_content.Add(key, result = GetValue());
			return result;
		}

		public int GetValue()
		{
			var key = unchecked(_keyIndex++);
			if (key != 0) return key;
			return HandleOverflow();
		}

		private int HandleOverflow()
		{
			_keyIndex = 0;
			throw new MaximumCustomOptionsCountReachedException();
		}
	}
}