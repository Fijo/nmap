using NMap.Service.Convert;

namespace NMap.Extentions
{
	public static class ConvertExtention
	{
		private static INConvert _service;
		
		internal static void Init(INConvert service)
		{
			_service = service;
		}

		public static TResult Convert<TSource, TResult>(this TSource source)
		{
			return _service.Convert<TSource, TResult>(source);
		}
	}
}