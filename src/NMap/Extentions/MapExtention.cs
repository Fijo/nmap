﻿using NMap.Service.Map;

namespace NMap.Extentions
{
	public static class MapExtention
	{
		private static INMap _service;
		
		internal static void Init(INMap service)
		{
			_service = service;
		}

		public static TResult Map<TSource, TResult>(this TSource source)
		{
			return _service.Map<TSource, TResult>(source);
		}
	}
}