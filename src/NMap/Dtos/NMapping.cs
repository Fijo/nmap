using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;
using JetBrains.Annotations;

namespace NMap.Dtos
{
	[Dto]
	public class NMapping<TMappingClass> where TMappingClass : class
	{
		public readonly Type Source;
		public readonly Type Result;
		public readonly int CustomOption;
		public readonly TMappingClass MappingClass;
		private readonly int _prefetchedHashCode;

		public NMapping(TMappingClass mappingClass, [NotNull] Type source, [NotNull] Type result, int customOption)
		{
			MappingClass = mappingClass;
			Source = source;
			Result = result;
			CustomOption = customOption;
			_prefetchedHashCode = ResolveHashCode();
		}

		private int ResolveHashCode()
		{
			return Source.GetHashCode().GetHashCodeAlgorithm(Result.GetHashCode(), CustomOption);
		}

		protected bool Equals([NotNull] NMapping<TMappingClass> other)
		{
			return Source == other.Source && Result == other.Result && CustomOption == other.CustomOption;
		}

		public override bool Equals(object obj)
		{
			return obj is NMapping<TMappingClass> && Equals((NMapping<TMappingClass>) obj);
		}

		public override int GetHashCode()
		{
			return _prefetchedHashCode;
		}
	}
}