using System;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;
using NMap.Dtos;
using NMap.Store;

namespace NMap.Factories
{
	public class NMappingFactory<TMappingClass> where TMappingClass : class
	{
		private readonly CustomOptionStore _customOptionStore = Kernel.Resolve<CustomOptionStore>();
		
		public NMapping<TMappingClass> CreateForRequest([NotNull] Type source, [NotNull] Type result, string customOption)
		{
			return Create(default(TMappingClass), source, result, customOption);
		}

		public NMapping<TMappingClass> Create(TMappingClass mappingClass, [NotNull] Type source, [NotNull] Type result, string customOption)
		{
			return new NMapping<TMappingClass>(mappingClass, source, result, _customOptionStore.Get(customOption));
		}
	}
}