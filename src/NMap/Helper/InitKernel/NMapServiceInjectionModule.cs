using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using NMap.Extentions;
using NMap.ImplBase.Convert;
using NMap.ImplBase.Map;
using NMap.Service.Base;
using NMap.Service.Convert;
using NMap.Service.Map;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;
using Ninject;

namespace NMap.Helper.InitKernel
{
	public class NMapServiceInjectionModule : IExtendedNinjectModule {
		public IKernel Kernel { get; private set; }
		public IDictionary<IExtendedInitHandler, object> ExtendedInit { get; set; }

		public void OnLoad(IKernel kernel)
		{
			Kernel = kernel;

			AddBindingsFor<IMapping, INMap>(kernel);
			AddBindingsFor<IConverting, INConvert>(kernel);
		}

		private void AddBindingsFor<TTMappingClass, TMapServiceInterface>(IKernel kernel) where TTMappingClass : class
		{
			kernel.Bind<TMapServiceInterface>().ToConstant((TMapServiceInterface) FijoCore.Infrastructure.DependencyInjection.InitKernel.Kernel.Resolve<INMapBase<TTMappingClass>>());
		}

		public void Init(IKernel kernel)
		{
			MapExtention.Init(kernel.Get<INMap>());
			ConvertExtention.Init(kernel.Get<INConvert>());
		}
		
		public void RegisterHandlers(IKernel kernel) {}

		public void Validate() {}
		
		public void Loaded(IKernel kernel) {}

		public void OnUnload(IKernel kernel) {}

		public string Name
		{
			get { return "NMapServiceInjectionModule"; }
		}
	}
}