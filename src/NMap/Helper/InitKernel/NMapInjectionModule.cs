﻿using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Store;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using NMap.Dtos;
using NMap.Factories;
using NMap.ImplBase.Convert;
using NMap.ImplBase.Map;
using NMap.Service.Base;
using NMap.Service.Convert;
using NMap.Service.Map;
using NMap.Store;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace NMap.Helper.InitKernel
{
    public class NMapInjectionModule : IExtendedNinjectModule {
        public IKernel Kernel { get; private set; }
    	public IDictionary<IExtendedInitHandler, object> ExtendedInit { get; set; }

    	public void OnLoad(IKernel kernel)
        {
            Kernel = kernel;


    		// ToDo add when injected into NMappingFactory - i need to extend my Resove function in the static Kernel for this
    		kernel.Bind<CustomOptionStore>().ToSelf().InRequestScope();
    		AddBindingsFor<IMapping, Service.Map.NMap, INMap>(kernel);
    		AddBindingsFor<IConverting, NConvert, INConvert>(kernel);
        }

    	private void AddBindingsFor<TTMappingClass, TMapService, TMapServiceInterface>(IKernel kernel) where TTMappingClass : class
		where TMapService : INMapBase<TTMappingClass>, TMapServiceInterface
    	{
    		kernel.Bind<IStore<NMapping<TTMappingClass>>>().To<HashtableStore<NMapping<TTMappingClass>>>().InSingletonScope();
    		kernel.Bind<NMappingFactory<TTMappingClass>>().ToSelf().InSingletonScope();
    		kernel.Bind<INMapBase<TTMappingClass>>().To<TMapService>().InSingletonScope();
    	}

    	public void Init(IKernel kernel) {}
		
    	public void RegisterHandlers(IKernel kernel) {}

    	public void Validate() {}
		
    	public void Loaded(IKernel kernel) {}

        public void OnUnload(IKernel kernel) {}

    	public string Name
        {
            get { return "NMapInjectionModule"; }
        }
    }
}