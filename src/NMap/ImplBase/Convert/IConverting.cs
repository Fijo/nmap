namespace NMap.ImplBase.Convert
{
	public interface IConverting<in TSource, out TResult> : IConverting
	{
		TResult Convert(TSource source);
	}

	public interface IConverting
	{
		object Convert(object source);
	}
}