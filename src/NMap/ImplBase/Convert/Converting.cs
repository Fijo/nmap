using NMap.ImplBase.Base;

namespace NMap.ImplBase.Convert
{
	public abstract class Converting<TSource, TResult> : NMappingRegistrationBase<IConverting, TSource, TResult>, IConverting<TSource, TResult>
	{
		#region Implementation of IConverting
		public abstract TResult Convert(TSource source);
		public abstract object Convert(object source);
		#endregion
	}
}