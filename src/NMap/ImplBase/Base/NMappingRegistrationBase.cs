using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NMap.Service.Base;

namespace NMap.ImplBase.Base
{
	public abstract class NMappingRegistrationBase<TMappingClass, TSource, TResult> where TMappingClass : class
	{
		protected virtual string CustomOption {get { return string.Empty; }}

		protected NMappingRegistrationBase()
		{
// ReSharper disable DoNotCallOverridableMethodsInConstructor
			Kernel.Resolve<INMapBase<TMappingClass>>().Register(GetSelf(), typeof (TSource), typeof (TResult), CustomOption);
// ReSharper restore DoNotCallOverridableMethodsInConstructor
		}

		protected internal abstract TMappingClass GetSelf();
	}
}