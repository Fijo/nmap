using NMap.ImplBase.Base;

namespace NMap.ImplBase.Map
{
	public abstract class Mapping<TSource, TResult> : NMappingRegistrationBase<IMapping, TSource, TResult>, IMapping<TSource, TResult>
	{
		#region Implementation of IMapping
		public abstract TResult Map(TSource source);
		public abstract object Map(object source);
		#endregion
	}
}