namespace NMap.ImplBase.Map
{
	public interface IMapping<in TSource, out TResult> : IMapping
	{
		TResult Map(TSource source);
	}

	public interface IMapping
	{
		object Map(object source);
	}
}